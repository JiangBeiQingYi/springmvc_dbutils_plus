package com.keta.generate.core;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keta.generate.util.Resources;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;

public abstract class AbstractGenerate
{
    protected Logger logger = LoggerFactory.getLogger(AbstractGenerate.class);
    
    protected Configuration config;
    
    protected String javaPath;
    
    protected String javaResourcesPath;
    
    protected String testJavaPath;
    
    protected String testJavaResourcesPath;
    
    protected String viewPath;
    
    protected String webappPath;
    
    protected String projectName;
    
    protected String separator;
    
    // 工程文件
    protected File projectFile;
    
    protected Map<String, Object> model;
    
    public AbstractGenerate()
    {
        init();
    }
    
    public void init()
    {
        try
        {
            // 获取文件分隔符
            separator = File.separator;
            // 获取工程路径
            projectFile = new File("code");
            String projectPath = Resources.TPL_PROJECT_DIR;
            logger.info("Project Path: {}", projectPath);
            // 项目名称
            projectName = StringUtils.substring(projectPath, projectPath.lastIndexOf(separator) + 1);
            logger.info("projectName : {}", projectName);
            // Java文件路径
            javaPath = StringUtils.replace(projectPath + "/", "/", separator);
            logger.info("Java Path: {}", javaPath);
            // 代码模板配置
            config = new Configuration();
            config.setDefaultEncoding("UTF-8");
            config.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");
            config.setDateFormat("yyyy-MM-dd");
            config.setTimeFormat("HH:mm:ss");
            config.setNumberFormat("#0.#");
            config.setObjectWrapper(new DefaultObjectWrapper());
            config.setClassForTemplateLoading(this.getClass(), "/template/" + Resources.TPL_FILE_DIR);
            // 定义模板变量
            model = new HashMap<String, Object>();
            model.put("packageName", Resources.TPL_PACKAGE_NAME);
            model.put("packagePath", Resources.TPL_PACKAGE_NAME.replace(".", "\\"));
            model.put("className", Resources.TPL_CLASS_NAME);
            model.put("instanceName", StringUtils.uncapitalize(Resources.TPL_CLASS_NAME));
            model.put("pknEntity", Resources.PKN_ENTITY);
            model.put("pknDAO", Resources.PKN_DAO);
            model.put("pknService", Resources.PKN_SERVICE);
            model.put("pknServiceImpl", Resources.PKN_SERVICE_IMPL);
            model.put("pknController", Resources.PKN_CONTROLLER);
            model.put("projectName", projectName);
            String dialect = "org.hibernate.dialect.MySQL5InnoDBDialect";
            if ("oracle.jdbc.driver.OracleDriver".equals(Resources.JDBC_DRIVER))
            {
                dialect = "org.hibernate.dialect.Oracle10gDialect";
            }
            model.put("dialect", dialect);
            model.put("driver", Resources.JDBC_DRIVER);
            model.put("dburl", Resources.JDBC_URL);
            model.put("username", Resources.JDBC_USERNAME);
            model.put("password", Resources.JDBC_PASSWORD);
            model.put("packName", Resources.TPL_PACKAGE_NAME);
            model.put("projectDir", Resources.TPL_PROJECT_DIR);
            model.put("projectSrcDir", Resources.TPL_PROJECT_DIR + "/src");
            model.put("tables", Resources.TPL_TABLE_NAME);
            model.put("date", new Date());
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
        }
    };
}
